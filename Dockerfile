FROM php:7.3
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y wget curl less zip unzip zlib1g-dev libzip-dev # libxslt1-dev (required by phpdocumentor when it''s included in the composer.json)
RUN docker-php-ext-install zip pdo_mysql # required by codeception
RUN echo "Installing nodejs..."
RUN apt-get install -y nodejs npm
RUN npm install -g npm@>=6.0.0
RUN echo "Installing phpdoc..."
RUN wget -nv -O phpdoc http://phpdoc.org/phpDocumentor.phar
RUN chmod +x phpdoc && mv phpdoc /usr/local/bin/phpdoc
RUN echo "Installing phploc..."
RUN wget -nv -O phploc https://phar.phpunit.de/phploc.phar
RUN chmod +x phploc && mv phploc /usr/local/bin/phploc
RUN echo "Installing phpcs..."
RUN wget -nv -O phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
RUN chmod +x phpcs && mv phpcs /usr/local/bin/phpcs
RUN echo "Installing composer..."
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN echo "Installing phpunit..."
RUN wget -nv -O phpunit https://phar.phpunit.de/phpunit-7.phar
RUN chmod +x phpunit && mv phpunit /usr/local/bin/phpunit
RUN echo "Installing codeception..."
RUN wget -nv -O codecept http://codeception.com/codecept.phar
RUN chmod +x codecept && mv codecept /usr/local/bin/codecept

