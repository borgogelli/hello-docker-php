## Overview

A docker image for wordpress plugin development

* https://hub.docker.com/r/borgogelli/hello-docker-php

## Build

### Automatically 

That image is builded automatically by the Bitbucket pipeline on every commit (push strategy)

* https://bitbucket.org/borgogelli/hello-docker-php/addon/pipelines/home

It is also possible to configure automatic compilation on cloud.docker.com (pull strategy)

* https://cloud.docker.com/repository/docker/borgogelli/hello-docker-php/builds

### Manually

To manually build the image from your docker server:

```
sudo docker login --username=borgogelli
sudo docker build --tag borgogelli/hello-docker-php:latest .
sudo docker push borgogelli/hello-docker-php:latest
```

...or more briefly:

```
npm docker
```